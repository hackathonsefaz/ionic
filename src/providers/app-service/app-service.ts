import { Injectable } from '@angular/core';
import { Headers } from "@angular/http";
import 'rxjs/add/operator/map';
import { Platform } from "ionic-angular";
import firebase from 'firebase';


@Injectable()
export class AppServiceProvider {

  public static AppName : string = "APP";
  public static AppVersion : string  = "0.0.1";
  static API_VERSION = '/api';
  public static cnpj = null;

  constructor(public platform: Platform) {}
  
  //**
  //Retrieve URL Accoring to param
  //*
  public getURL(addr){
    //let BASE_URL_SERVER = (this.platform.is('core')) ? '' : 'http://45.55.65.14/';
    let BASE_URL_SERVER = 'http://45.55.65.14';
    let SERVICE_ADDR = `${BASE_URL_SERVER}${AppServiceProvider.API_VERSION}/`;
    let url = '';
    switch(addr){
      case 'BASE_URL_SERVER':
        url = BASE_URL_SERVER;
        break;
      case 'SERVICE_ADDR':
        url = SERVICE_ADDR;
        break;
      case 'ROUTER_BASE_SEARCH':
        url = `${SERVICE_ADDR}search/`;
        break;
        case 'ROUTER_BASE_LIKE':
        url = `${SERVICE_ADDR}products/favorite/`;
        break;
        case 'ROUTER_BASE_TRACK':
        url = `${SERVICE_ADDR}products/tracking`;
        break;
      default:
        url =  BASE_URL_SERVER;
    }
    return url;
  }

  public static getHeaders(){
      let headersObject = new Headers()
      headersObject.append('Content-Type', 'application/json');
      headersObject.append('X-ClientId', localStorage.getItem('clientId'));
      return {headers: headersObject};
  }

  
}
