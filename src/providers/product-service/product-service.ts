import { Injectable } from '@angular/core';
import { Http, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';
import { ReplaySubject } from "rxjs";
import { AppServiceProvider } from "../app-service/app-service";

/*
  Generated class for the ProductServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ProductServiceProvider {

  constructor(
    public http: Http,
    public ASP: AppServiceProvider
  ) {
    console.log('Hello ProductServiceProvider Provider');
  }

  like(codigo){
    let project = new ReplaySubject(1);
    let options = new RequestOptions({ headers: AppServiceProvider.getHeaders().headers});
    this.http.post(`${this.ASP.getURL('ROUTER_BASE_LIKE')}${codigo}`,null,options).subscribe( response => {
        project.next(response.json());
    });
    return project;
  }
  unLike(){}
  track(product,expectedValue){
    let project = new ReplaySubject(1);
    let params = {
      "barcode": product.codGetin,
      "latitude": product.numLatitude,
      "longitude": product.numLongitude,
      "cnpj": product.numCNPJ,
      "current_price": product.valUltimaVenda,
      "expected_price": expectedValue
    };
    //let paramsStr = JSON.stringify(params);
    let options = new RequestOptions({ headers: AppServiceProvider.getHeaders().headers});
    this.http.post(`${this.ASP.getURL('ROUTER_BASE_TRACK')}`,params,options).subscribe( response => {
        project.next(response.json());
    });
    return project;
  }
  unTrack(){}

}
