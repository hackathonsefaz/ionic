import { Injectable } from '@angular/core';
import { Http, RequestOptions, Request } from "@angular/http";
import 'rxjs/add/operator/map';
import { ReplaySubject } from 'rxjs';
import { AppServiceProvider } from "../app-service/app-service";

/*
  Generated class for the SearchServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SearchServiceProvider {
  constructor(
    public http: Http,
    public ASP: AppServiceProvider
  ) {}

  searchByDescription(searchParams){
    let project = new ReplaySubject(1);
    let params = JSON.stringify(searchParams);
    let options = new RequestOptions({ headers: AppServiceProvider.getHeaders().headers});
    this.http.post(`${this.ASP.getURL('ROUTER_BASE_SEARCH')}description`,params,options).subscribe( response => {
        project.next(response.json());
    });
    return project;
  }

  searchByBarCode(searchParams){
    let project = new ReplaySubject(1);
    let params = JSON.stringify(searchParams);
    let options = new RequestOptions({ headers: AppServiceProvider.getHeaders().headers});    
    this.http.post(`${this.ASP.getURL('ROUTER_BASE_SEARCH')}barcode`,params,options).subscribe( response => {
        project.next(response.json());
    });
    return project;
  }
}
