import { Component, ViewChild, trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import firebase from 'firebase';


/**
 * Generated class for the SliderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
  animations: [
    
  trigger('bounce', [
        state('*', style({
            transform: 'translateX(0)'
        })),
        transition('* => rightSwipe', animate('700ms ease-out', keyframes([
          style({transform: 'translateX(0)', offset: 0}),
          style({transform: 'translateX(-65px)',  offset: 0.3}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ]))),
        transition('* => leftSwipe', animate('700ms ease-out', keyframes([
          style({transform: 'translateX(0)', offset: 0}),
          style({transform: 'translateX(65px)',  offset: 0.3}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ])))
    ])
  ]
})

export class SliderPage {
    @ViewChild(Slides) slides: Slides;
    public state: string = 'x';


		slide = [
    {
      title: "Você deu o 1º passo para fazer a melhor compra",
      description: "As melhores ofertas de onde você estiver",
      image: "assets/img/slider-01.svg",
    },
    {
      title: "Nuca mais perca uma oferta!",
      description: "Notificação em tempo real",
      image: "assets/img/slider-02.svg",
    },
    {
      title: "Rota até o produto Desejado",
      description: "Trajeto até o produto ofertado",
      image: "assets/img/slider-03.svg",
    }
  ];

  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      if (localStorage.getItem('clientId')) {
          this.jumpTo('Home');
      }
  }

  slideMoved() {
      if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex())
        this.state = 'rightSwipe';
      else
        this.state = 'leftSwipe';
    }

    animationDone(){
      this.state = 'x';
    }

  /*ionViewDidLoad() {
     // console.log(this.getUser());
    console.log('ionViewDidLoad SliderPage');
  }*/

  jumpTo(pagePrefix){
    this.navCtrl.setRoot(`${pagePrefix}Page`);
  }
}
