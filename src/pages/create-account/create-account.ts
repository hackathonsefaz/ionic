import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import firebase from 'firebase';

/**
 * Generated class for the CreateAccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-account',
  templateUrl: 'create-account.html',
})
export class CreateAccountPage {

  private user = {};

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  createAccount(user) {
  	firebase.auth().createUserWithEmailAndPassword(user.email,user.password).catch(function(error) {
    // Handle Errors here.
    console.log(error);
  	// ...
	});
    this.getUser(this.navCtrl);
  }

  jumpTo(pagePrefix){
    this.navCtrl.push(`${pagePrefix}Page`);
  }

  getUser(navCtrl) {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {

        localStorage.setItem('clientId', user.uid);
        if (user.uid != '') {
          navCtrl.setRoot('HomePage');
        }

      } else {
        console.log('Usuário não encontrado.');
      }
    });
  }

  setToken() {
    var user = firebase.auth().currentUser;
    localStorage.setItem('clientId', user.uid);
  }
  
  /*ionViewDidLoad() {
    this.getUser();
  }*/

}
