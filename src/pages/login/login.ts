import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private user = {};

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  jumpTo(pagePrefix){
    this.navCtrl.push(`${pagePrefix}Page`);
  }

  login(user) {
    firebase.auth().signInWithEmailAndPassword(user.email, user.password).catch(function(error) {
      console.log(error);
    });
    this.getUser(this.navCtrl);
  }

  anonymousLogin() {
    firebase.auth().signInAnonymously().catch(function(error) {
      console.log(error);
    });
    this.getUser(this.navCtrl);
  }

  logout() {
    firebase.auth().signOut().then(function() {
      console.log('Bye');
      localStorage.removeItem('clientId');
    }, function(error) {
      console.log(error);
    });
  }

  getUser(navCtrl) {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {

        localStorage.setItem('clientId', user.uid);
        if (user.uid != '') {
          navCtrl.setRoot('HomePage');
        }

       } else {
        console.log('Usuário não encontrado.');
      }
    });
  }

  setToken() {
       var user = firebase.auth().currentUser;
    console.log(user);
    //localStorage.setItem('clientId', user.uid);
  }

}
