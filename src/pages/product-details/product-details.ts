import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ToastController, AlertController, Platform } from "ionic-angular";
import { Screenshot } from '@ionic-native/screenshot';
import { ProductServiceProvider } from "../../providers/product-service/product-service";


/**
 * Generated class for the ProductDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-product-details",
  templateUrl: "product-details.html"
})
export class ProductDetailsPage {
  product: any;

  tab: string = "precos";
  isAndroid: boolean = false;

  slides = [
    {
      title: "",
      description: "",
      image: "assets/img/img-especific.png"
    },
    {
      title: "",
      description: " ",
      image: "assets/img/img-especific.png"
    },
    {
      title: "",
      description: "",
      image: "assets/img/img-especific.png"
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing,
    private screenshot: Screenshot,
    private toastCtrl: ToastController,
    private PSP: ProductServiceProvider,
    private alertCtrl: AlertController,
    private platform: Platform
  ) {
    this.product = this.navParams.get("product");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductDetailsPage");
  }

  share() {
    //console.log('Sharing');

    this.screenshot
      .URI(80)
      .then(res => {
        //console.log(res);
        this.socialSharing
          .shareViaWhatsApp(null, res.URI, null)
          .then(() => {
            let toast = this.toastCtrl.create({
              message: "Mensagem enviada com sicesso",
              duration: 3000,
              position: "top"
            });

            toast.onDidDismiss(() => {
              console.log("Dismissed toast");
            });

            toast.present();
          })
          .catch(err => console.log(err));
      })
      .catch(error => console.log(error));
  }

  like(product) {
    this.PSP.like(product.codGetin).subscribe((response: any) => {
      let toast = this.toastCtrl.create({
        message: response.message,
        duration: 3000,
        position: "top"
      });

      toast.onDidDismiss(() => {
        console.log("Dismissed toast");
      });

      toast.present();
    });
  }

  track(product) {
    let prompt = this.alertCtrl.create({
      title: "Monitore o valor do produto",
      message: "Digite o valor desejado",
      inputs: [
        {
          name: "value",
          placeholder: "VALOR",
          type: "number"
        }
      ],
      buttons: [
        {
          text: "Ok",
          handler: data => {
            this.PSP.track(product, data.value).subscribe((response: any) => {
              console.log(response);
              let toast = this.toastCtrl.create({
                message: response.message,
                duration: 3000,
                position: "top"
              });

              toast.onDidDismiss(() => {
                console.log("Dismissed toast");
              });

              toast.present();
            });
          }
        }
      ]
    });
    prompt.present();
  }

  navigate(product) {
    let destination = product.numLatitude + "," + product.numLongitude;

    if (this.platform.is("ios")) {
      window.open("maps://?q=" + destination, "_system");
    } else {
      let label = encodeURI(product.nomFantasia);
      window.open("geo:0,0?q=" + destination + "(" + label + ")", "_system");
    }
  }
}
