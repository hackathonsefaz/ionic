import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  public products = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.products = this.navParams.get('products');
  }

  ionViewDidLoad() {
    console.log(this.products);
  }
  showDetails(product){
    this.navCtrl.push('ProductDetailsPage',{
      'product' : product
    });
  }
}
