import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppServiceProvider } from "../../providers/app-service/app-service";
import { SearchServiceProvider } from "../../providers/search-service/search-service";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LoadingController } from 'ionic-angular/index';

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  brightness: number = 20;
  contrast: number = 0;
  warmth: number = 1300;
  structure: any = { lower: 33, upper: 60 };
  text: number = 0;
  queryParams :any = {
    radius : 15
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public SSP: SearchServiceProvider,
    public barcodeScanner: BarcodeScanner,
    public loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {}

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  scanBarcode(){
    this.barcodeScanner.scan().then((barcodeData) => {
      this.queryParams.queryText = barcodeData.text;
      this.searchHandler(this.queryParams);
     }, (err) => {
         // An error occurred
     });
  }

  searchHandler(parameters){
    this.presentLoadingDefault();
    
    let isNum = /^\d+$/.test(parameters.queryText);
    let params = {
      "dias": 3,
      "latitude": -9.6432331,
      "longitude": -35.7190686,
      "raio": (parameters.radius) ? parameters.radius : 15
    };
    if(isNum){
      params['codigoDeBarras'] = parameters.queryText;
      this.SSP.searchByBarCode(params).subscribe( (response:any) => {
        this.navCtrl.push('ResultPage',{
          products : response
        });
      });
    }else{
      params['descricao'] = parameters.queryText;
      this.SSP.searchByDescription(params).subscribe( (response:any) => {
        this.navCtrl.push('ResultPage',{
          'products' : response
        });
      });
    }
  }

}
