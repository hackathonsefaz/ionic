import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CreateAccountPage } from '../pages/create-account/create-account';
import { SliderPage } from '../pages/slider/slider';
import { ResultPage } from '../pages/result/result';
import { ProductDetailsPage } from '../pages/product-details/product-details';
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = SliderPage;
  pages: Array<{ title: string; component: any }>;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    firebase.initializeApp({
      apiKey: "AIzaSyCWNNEO_KZpPS--Xm2_3eO68306smujM4U",
      authDomain: "economizei-f6bc5.firebaseapp.com",
      databaseURL: "https://economizei-f6bc5.firebaseio.com",
      projectId: "economizei-f6bc5",
      storageBucket: "economizei-f6bc5.appspot.com",
      messagingSenderId: "32864838367"
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      { title: "", component: SliderPage },
      { title: "HomePage", component: HomePage },
      { title: "Login", component: LoginPage },
      { title: "CreateAccount", component: CreateAccountPage },
      { title: "ResultPage", component: ResultPage },
      { title: "ProductDetailsPage", component: ProductDetailsPage }
    ];
  }

  logout() {
    window.localStorage.clear();
    this.nav.setRoot(LoginPage);
  }
}

