import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { SliderPage } from '../pages/slider/slider';
import { AppServiceProvider } from '../providers/app-service/app-service';
import { SearchServiceProvider } from '../providers/search-service/search-service';
import { HttpModule } from "@angular/http";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { SocialSharing } from "@ionic-native/social-sharing";
import { Screenshot } from "@ionic-native/screenshot";
import { LoginPage } from "../pages/login/login";
import { ProductServiceProvider } from '../providers/product-service/product-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [MyApp, SliderPage],
  imports: [BrowserModule, IonicModule.forRoot(MyApp), HttpModule, BrowserAnimationsModule],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, SliderPage],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AppServiceProvider,
    SearchServiceProvider,
    SocialSharing,
    Screenshot,
    BarcodeScanner,
    ProductServiceProvider
  ]
})
export class AppModule {}
